# Notes aux concours aux grandes écoles (*2009/2010* et *2010/2011*)
### [Lilian BESSON](http://perso.crans.org/besson) (ancien élève du *Lycée Thiers* de *Marseille*)

- - -
## Petites Mines *(2009/2010)*
##### Épreuves écrites
 * Physique : 16.96/20;
 * Mathématiques 1 : 17.42/20;
 * Mathématiques 2 : 17.22/20;
 * Francais : 12.85/20;
 * Rang : **24°/2042**.

##### Épreuves orales
 * Entretient : 20/20;
 * Mathématiques : 16/20;
 * Anglais : 14/20;
 * Physique : 13/20;
 * Rang : **19°/1478**.

### Admis (mais refusé) à l'École des Mines d'Alès. Rang : **19°**.

- - -
## École Polytechnique *(2010/2011)*
##### Épreuves écrites
 * Mathématiques 1 (A) : 15/20;
 * Mathématiques 2 (B) : 16,1/20;
 * Physique-SI : 15,9/20;
 * Physique : 13,1/20;
 * Informatique : 15,2/20;
 * Français : 15/20;
 * Langue Vivante (Anglais) : 8/20;

### Admis (mais refusé). Rang : **81°**.

- - -
## Écoles Normales Supérieures *(2010/2011)*
##### Épreuves écrites
 * Mathématiques (A) LC = 15/20;
 * Mathématiques (B) ULC = 13,7/20;
 * Physique ULC = 13,1/20;
 * Informatique C = 15,2/20;
 * Français ULC = 15/20;
 * Langue Vivante 1 (Anglais) ULC = 8/20;
 * Langue Vivante 2 (Espagnol) ULC = 14/20;

### *ENS Ulm* : Admissible (non admis). Rang : **84°**.
### *ENS Cachan* : Admis élève normalien. Rang : **99°**.
### *ENS Lyon* : Admis (mais refusé). Rang : **143°**.

- - -
## Écoles Centrales *(2010/2011)*
##### Épreuves écrites
 * Langue vivante (Anglais) : 6,4/20;
 * Mathématiques 1 : 17,8/20;
 * Mathématiques 2 : 16,5/20;
 * Physique : 15,9/20;
 * Physique-Chimie : 15,5/20;
 * Rédaction : 15,1/20;
 * Sciences Industrielles : 11,5/20;

##### Épreuves orales
 * Langue vivante 1 (Anglais) = 17/20;
 * Mathématiques 1 = 18/20;
 * Mathématiques 2 = 18/20;
 * Physique 1 = 14/20;
 * Physique 2 = 17/20;
 * Chimie = 16/20;
 * Travaux Pratiques Physique = 17/20;
 * TIPE = 17,5/20.

### Admis mais refusé
 * Rang : **26°** à l'École Centrale de Paris;
 * Rang : **28°** à SupÉlec;
 * Rang : **22°** à l'École Centrale de Lyon;
 * Rang : **14°** à SupOptique;
 * Rang : **8°** à l'École Centrale de Lille;
 * Rang : **7°** à l'École Centrale de Nantes;
 * Rang : **16°** à l'École Centrale de Marseille;


- - -
## Banque Mines/Pont *(2010/2011)*
##### Épreuves écrites
 * Mathématiques 1 : 18,25/20;
 * Physique 1 : 13,25/20;
 * LV Anglais : 9,5/20;
 * Mathématiques 2 : 16,25/20;
 * Sciences Industrielles : 16,5/20;
 * Français : 10/20;
 * Physique 2 : 18,75/20;
 * Chimie : 9,5/20.


- - -
## Télécom Sud-Paris *(2010/2011)*
 + Entretient: 20/20;
 + Admis (mais refusé). Rang = **2°/?**.


- - -
##### (Par [Lilian Besson](http://fb.me/naereencorp.lbesson)). Toutes remarques ou commentaires sont les bienvenus [par email](lilian.besson{AT@AT]normale[DOT.POINT}fr) ou [via bitbucket](https://bitbucket.org/lbesson/agreg/issues/new).